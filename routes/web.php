<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

    //social login
    $router->get('auth/{provider}', 'AuthController@redirectToProvider');
    $router->get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login', 'AuthController@login');

    $router->get('user',  ['uses' => 'UserController@showAllAuthors']);
  
    $router->get('user/{id}', ['uses' => 'UserController@showOneAuthor']);
  
    $router->post('user', ['uses' => 'UserController@create']);
  
    $router->delete('user/{id}', ['uses' => 'UserController@delete']);
  
    $router->post('user/{id}', ['uses' => 'UserController@update']);

    $router->post('user/password/{id}', ['uses' => 'UserController@passwordUpdate']);
  });
