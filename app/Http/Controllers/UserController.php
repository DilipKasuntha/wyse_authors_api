<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    public function login(Request $request)
    {
          //validate incoming request 
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function showAllAuthors()
    {
        return response()->json(User::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(User::find($id));
    }

    public function create(Request $request)
    {
    try {

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $plainPassword = $request->input('password');
        $user->password = app('hash')->make($plainPassword);
        $user->provider=$request->provider;
        $user->provider_id=$request->provider_id;
        $user->avatar=$request->avatar;
        $user->user_type=$request->user_type;
        $user->save();

        return response()->json(['user' => $user, 'message' => 'CREATED'], 200);
    } catch (\Exception $e) {
        //return error message
        return response()->json(['message' => $e->getmessage()], 409);
   }
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => ['email',Rule::unique('users')->ignore($id)],
        ]);

        $author = User::findOrFail($id);
        $author->update($request->all());

        return response()->json(['user'=>$author,'message'=>'UPDATED'], 200);
    }
    public function passwordUpdate($id, Request $request)
    {
        try {

            $this->validate($request, [
                'password' => 'required',
            ]);
            $user =User::where('id',$id)->first();
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->save();
    
            return response()->json(['user' => $user, 'message' => 'PassWordUpdated'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getmessage()], 409);
       }

    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}